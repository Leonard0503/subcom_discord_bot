var fs = require('fs');
const Discord = require('discord.js');
const { start } = require('repl');
const client = new Discord.Client()
var key = "";
key = fs.readFileSync('key.txt', 'utf8');
var announce_channel;
const progress_total = 7;

function timeoutput(){
  let currentdate = new Date();
  let grouparr = []
  grouparr.push(currentdate.getHours().toString());
  grouparr.push(currentdate.getMinutes().toString());
  grouparr.push(currentdate.getSeconds().toString());
  grouparr.push(currentdate.getMilliseconds().toString());
  for (i = 0; i <= 2; i++){
    if (grouparr[i].length == 1){
      grouparr[i] = "0" + grouparr[i];
    }
    grouparr[i] = grouparr[i] + ":"
  }
  while (grouparr[3].length <= 2){
    grouparr[3] = "0" + grouparr[3];
  }
  var re_value = ""
  for (i = 0; i < grouparr.length; i++){
    re_value = re_value + grouparr[i];
  }
  return re_value;
}

function output_progress(){
  var Embedformat = new Discord.MessageEmbed()
    .setTitle("Progress of Clearing Virus")
    .setColor("#f96f45")
    .setDescription("See how every team is performing")
    .setFooter("Time Updated | " + timeoutput());
    for (j = 0; j < allteam.length; j++){
      output_string = "[ "; 
      for (k = 0; k < (allteam[j].progress / 10); k++){
        output_string = output_string + "▇";
      }
      for (k = 0; k < (10 - (allteam[j].progress / 10)); k++){
        output_string = output_string + "░░";
      }
      output_string = output_string + " ] " + allteam[j].progress + "%"; //add extra space at the end for beauty effect
      Embedformat.addField("Team " + (j + 1), output_string)
    }
  return Embedformat;
}

function checkprogressskip(progress_level,ref_progress){
  if (progress_level - 1 == ref_progress){
    return true;
  }
  else{
    return false;
  }
}

function output_allteamandplayer(){
  var Embedformat = new Discord.MessageEmbed()
    .setTitle("All Teams and Players")
    .setColor("#25ff79")
    .setDescription("Your device is being infected with virus...")
    .setFooter("Time Updated | " + timeoutput());
  var players_in_team = [];
  var num_of_players = 0;
  for (i = 0; i < allteam.length; i++){
    players_in_team.push("");
    num_of_players = 0;
    for (j = 0; j < allplayer.length; j++){
      if (allplayer[j].team == i){
        if (num_of_players > 0){players_in_team[i] = players_in_team[i] + ", "}
        players_in_team[i] = players_in_team[i] + allplayer[j].username;
        num_of_players++;
      }
    }
    if (players_in_team[i] != ""){Embedformat.addField("Team " + (i + 1), players_in_team[i]);}    
  }
  return Embedformat;
}

function progress_update(author_id,progress_rate,re_channel,progress_level){
  for (var i = 0; i < allplayer.length; i++){
    if (allplayer[i].ID == author_id){
      if (allteam[allplayer[i].team].progress >= progress_rate){ //The team has already solved the phrase
        re_channel.send("Your team has already solved this phrase");
        return false; //solved already, break for the function
      }
      else if (!checkprogressskip(progress_level,allteam[allplayer[i].team].finished_progress)){
        return;
      }
      else{
        allteam[allplayer[i].team].progress = progress_rate;
        console.log("team" + i + " updated");
        try{
          announce_channel.send(output_progress());
        }
        catch(err){
          console.warn("error, probably did not set announcement channel");
          return false; //error occur, skip function
        }
        allteam[allplayer[i].team].finished_progress++;
        return true;
        }  
      } 
  }
}

function lowerandnospace(ref){
  var re = ref.toLowerCase();
  re = re.replace(/ /g,'');
  if (re.includes("”") || re.includes("“")){
    var list = re.split(/”|“/);
    var temp = list[0] + "\"";
    for (i = 1; i < list.length; i++){
      temp = temp + list[i];
      if (i + 1 < list.length){
        temp = temp + "\"";
      }
    }
    re = temp;
  }
  return re;
}
function isadmin(messageauthor){
  if (messageauthor.member.roles.cache.find(r => r.name === "test") || messageauthor.member.roles.cache.find(r => r.name === "Staff")){
    return true;
  }
  else{return false}
}

class TeamProgress{
  constructor(progress,finished_progress) {
    this.progress = progress;
    this.finished_progress = finished_progress;
  }
}

class PlayerInfo{
  constructor(team,ID,username) {
    this.team = team;
    this.ID = ID;
    this.username = username
  }
}

var allplayer = [];
//set team progress to 0
var allteam = [ //at least one team has to be initiated
new TeamProgress(0,0)];
//end of setting team progress


client.on('message', (receivedMessage) => { //phrase 1
  // Prevent bot from responding to its own messages
  if (receivedMessage.author == client.user) {
      return
  }
  check_riddle = lowerandnospace(receivedMessage.content);
  var splited_msg = receivedMessage.content.split(" ");
  if (splited_msg[0] == ";;assign" && isadmin(receivedMessage)){ //assign function
    if (splited_msg.length == 1){
      receivedMessage.channel.send("`;;assign {team_num(from 0 to inf)} {@user}`");
      receivedMessage.channel.send("`This command assign a user to a team`");
      return; //ask for help only
    }
    var teamnum = parseInt(splited_msg[1]); //try converting string to int
    try{
      assign_user = splited_msg[2].slice(3,(splited_msg[2].length - 1));
      if ((allteam.length - 1) < teamnum){ //if the number of team is less then teamnum, it means the team doesn't exist
        receivedMessage.channel.send("Only " + allteam.length + " teams exist!");
        return; //exit as command cannot run corretly
      }
      assign_user_detail = client.users.cache.get(assign_user);
      let player = new PlayerInfo(teamnum,assign_user,assign_user_detail.username);
      allplayer.push(player); 
      announce_channel.send(output_allteamandplayer());
    }
    catch(err){
      receivedMessage.channel.send("Team number inputted is not an integer, or team number is invalid!");
      console.log(err);
      return; //exit as command cannot run corretly
    }    
  }

  else if (receivedMessage.content == ";;maketeam" && isadmin(receivedMessage)){ //maketeam function
    let newteam = new TeamProgress(0,0);
    allteam.push(newteam);
    receivedMessage.channel.send("Created team " + (allteam.length - 1));
  }

  else if (receivedMessage.content == ";;announce_channel" && isadmin(receivedMessage)){
    announce_channel = receivedMessage.channel
    receivedMessage.channel.send("This is the announcement channel");
    console.log("set announcement channel as ID : " + announce_channel.id);
  }

  else if (check_riddle == "tefwvhhhhiutpiyx\"rptiftleefwv\"tirrpt"){ //phrase 1
    booloutput = progress_update(receivedMessage.author.id,20,receivedMessage.channel,1);
    if (booloutput){
      receivedMessage.channel.send("**Solved phrase 1!**");
      receivedMessage.channel.send("*You may want to keep this...*");
    }  
  }

  else if (check_riddle == "caesar"){ //phrase 2.1
    booloutput = progress_update(receivedMessage.author.id,30,receivedMessage.channel,2);
    if (booloutput){
      receivedMessage.channel.send("Solved phrase 2.1");
      receivedMessage.channel.send("*left shift 6*");  
    }
  }

  else if (check_riddle == "monoalphabetic" || check_riddle == "monoalphabeticsubstitution"){ //phrase 2.2
    booloutput = progress_update(receivedMessage.author.id,40,receivedMessage.channel,3);
    if (booloutput){
    receivedMessage.channel.send("Solved phrase 2.2");
    receivedMessage.channel.send("<https://imgur.com/KEtCCCJ>");
    }
  }

  else if (check_riddle == "route"){ //phrase 2.3 and 2
    booloutput = progress_update(receivedMessage.author.id,60,receivedMessage.channel,4);
    if (booloutput){
      receivedMessage.channel.send("Solved phrase 2.3");
      receivedMessage.channel.send("**Solved all phrase 2!**");
      receivedMessage.channel.send("<https://imgur.com/0xVKiHQ>");
    }
  }

  else if (check_riddle == "nyzqpbbbbconjcsr\"ljncznfyyzqp\"nclljn"){ //phrase 3.1
    booloutput = progress_update(receivedMessage.author.id,70,receivedMessage.channel,5);
    if (booloutput){
    receivedMessage.channel.send("Solved phrase 3.1");
    }
  }

  else if (check_riddle == "smondbbbbetsaelp\"yaseosummond\"seyyas"){ //phrase 3.2
    booloutput = progress_update(receivedMessage.author.id,90,receivedMessage.channel,6);
    if (booloutput){
    receivedMessage.channel.send("Solved phrase 3.2");
    }
  }

  else if (check_riddle == "say\"pleasesayyes\"tosummondesmondbb" || check_riddle == "say\"pleasesayyes\"tosummondesmondbbbb"){ //phrase 3.3 and game
    booloutput = progress_update(receivedMessage.author.id,100,receivedMessage.channel,7);
    if (booloutput){
      receivedMessage.channel.send("Solved phrase 3.3");
      receivedMessage.channel.send("**Congrats! The virus on your device has been removed!**");
    }
  }

})

client.on('ready', () => {
    console.log("Connected as " + client.user.tag)
})

//Bot invite link: https://discord.com/oauth2/authorize?client_id=772800607441387531&scope=bot
client.login(key)
